<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $language->language ?>" lang="<?php echo $language->language ?>" dir="<?php echo $language->dir ?>">
  <head>
    <?php
    global $user;
    ?>
    <title><?php print $head_title; ?></title>
    <?php print $head; ?>
    <?php print $styles; ?>
    <!--[if lte IE 8]>
    <style type="text/css" media="all">@import "<?php echo $base_path . path_to_theme() ?>/ie.css";</style>
    <![endif]-->
    <!--[if lte IE 6]>
    <style type="text/css" media="all">@import "<?php echo $base_path . path_to_theme() ?>/ie6.css";</style>
    <![endif]-->
    <?php print $scripts; ?>
    <style type="text/css" media="screen">
      .menu-name-navigation .menu{
        -moz-border-radius:5px;
        -webkit-border-radius:5px;
        border-radius:5px;
        -webkit-box-shadow:1px 1px 3px #888;
        -moz-box-shadow:1px 1px 3px #888;
        width: 105%;
        list-style-image: none !important;
        list-style: none;
        margin-bottom: 5px;
      }
      .menu-name-navigation .menu li{border-bottom:1px solid #6E737B; margin-left:4px; margin-right: 3px;
                                     list-style-image: none !important;
                                     list-style: none !important;
      }
      .menu-name-navigation .menu ul li, .menu-name-navigation .menu li:last-child{border:none;
      }
      .menu-name-navigation li a{
        display:block;
        color:#FFF;
        text-decoration:none;
        font-family:'Helvetica', Arial, sans-serif;
        font-size:13px;
        padding:3px 5px;
        text-shadow:1px 1px 1px #325179;
      }
      .menu-name-navigation .menu a:hover{
        color:#F9B855;
        -moz-border-radius:5px;
        -webkit-border-radius:5px;
        border-radius:5px;
        -webkit-box-shadow:1px 1px 3px #888;
        -moz-box-shadow:1px 1px 3px #888;
        -webkit-transition: color 0.2s linear;

      }
      .menu-name-navigation .menu  li a.active{

        color: #F8BA64 !important;

      }
      .menu-name-navigation .menu ul a{background-color:#A6A7A2;}
      .menu-name-navigation .menu ul a:hover{
        background-color:#FFF;
        color:#2961A9;
        text-shadow:none;
        -webkit-transition: color, background-color 0.2s linear;
      }
      .menu-name-navigation ul{
        /*display:block;*/
        background-color:#595F6A;
        margin:0;
        padding:0;

      }
      .menu-name-navigation .menu ul{background-color:#A6A7A2;}
      .menu-name-navigation .menu li ul {/*display:none;*/}
      .menu-name-navigation .menu li.expanded a{
        font-weight: bold;
        color: #fff;
      }
      .menu-name-navigation .menu li.expanded a:hover{
        color:#F9B855;
      }
      .menu-name-navigation .menu li.expanded ul li a:hover{
        color:#2961A9;
      }
      <?php print (preg_match("/devel/i", $_SERVER['SERVER_NAME'])) ? print "#branding { background-color: #CCFF88 } "  : NULL;
      ?></style>
  </head>
  <body class="<?php print $body_classes; ?>">
    <div id="skip-link">
      <a href="#main-content"><?php print t('Skip to main content'); ?></a>
    </div>
    <div class="element-invisible">
      <a id="main-content"></a>
    </div>
    <div id="branding" class="clearfix">
      <a href="<?php echo $base_path ?>" >
        <img src="<?php echo $base_path . path_to_theme() ?>/images/logo.png" alt="" >
      </a>
      <div style="font-size: 10px;">&copy;<?php print date('Y') ?>, Markinson Satch, Inc</div>
      <?php if (!empty($breadcrumb)): ?><?php print $breadcrumb; ?><?php endif; ?>

      <?php if (!empty($primary_local_tasks)):
        ?><ul class="tabs primary"><?php print $primary_local_tasks; ?></ul><?php endif; ?>
    </div>
    <div id="page">
      <?php if (!empty($secondary_local_tasks)):
        ?><ul class="tabs secondary"><?php print $secondary_local_tasks; ?></ul><?php endif; ?>

      <div id="content" class="clearfix">
        <?php if (!empty($messages)):
          ?>
          <div id="console" class="clearfix">
            <?php print $messages; ?>
          </div>
        <?php endif; ?>

        <?php if (!empty($action_links)):
          ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>

        <?php if (!$is_admin && $user->uid != 0) {
          ?>
          <div class="container half-left">
            <div class="conthead">
              <h2>Menu Editor</h2>
            </div>
            <div class="contentbox">
              <?php print createMenublock(); ?>
            </div>
          </div>
        <?php } ?>

        <div class="container" style="<?php ($is_admin || $user->uid == 0) ? print "width: 100%"  : NULL; ?>;">
          <div class="conthead">
            <h2><?php print $title
        ?></h2>
          </div>
          <div class="contentbox">
            <?php print $content
            ?>
          </div>
        </div>
        <?php if (!empty($help)):
          ?>
          <div id="help" style="color:#D8D8D8; a:#72A5FF;">
            <?php print $help; ?>
          </div>
        <?php endif; ?>
      </div>
      <div id="footer">
        <?php if (!empty($feed_icons)): ?><?php print $feed_icons; ?><?php endif; ?>
      </div>
    </div>
    <?php print $closure; ?>
  </body>
</html>
